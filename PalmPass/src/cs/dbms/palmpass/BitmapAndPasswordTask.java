package cs.dbms.palmpass;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.TextView;

public class BitmapAndPasswordTask extends AsyncTask<ResultsContainer, Integer, ResultsContainer>{

	private ResultsContainer mContainer;
	TextView mTextView;
	ImageView mImageView;
	int mLength;
	private Bitmap bmp;
	Context mContext;
	String mPseudo;
	
	ProgressDialog progress;
	int prog;
	
	private static final int PROG_GREY = 1;
	private static final int PROG_GX = 2;
	private static final int PROG_GY = 3;
	private static final int PROG_ADD = 4;
	private static final int PROG_STRING = 5;
	
	private static final int SOBEL_X = 1;
	private static final int SOBEL_Y = 0;
	
	private final static double[][] SOBEL_KERNEL_X = new double[][]{
		{ -1, 0, 1 },
		{ -2, 0, 2 },
		{ -1, 0, 1}};
	
	private final static double[][] SOBEL_KERNEL_Y = new double[][]{
		{  1, 2, 1 },
        {  0, 0, 0 },
        { -1,-2,-1}};
	
	private final static double CHAR_MIN = 48;
	private final static double CHAR_MAX = 122;

	public BitmapAndPasswordTask (Context context, ResultsContainer container){
		mContext = context;
		mContainer = container;
	}
	
	@Override
	protected void onPreExecute(){
		//Sets the progress screen before starting the computation
		progress = new ProgressDialog(mContext);
		progress.setCancelable(false);
		progress.setMessage("Rotating... (1/5)");
		progress.show();
	}
	
	@Override
	protected ResultsContainer doInBackground(ResultsContainer... params) {
		
		bmp = mContainer.getBitmap();
		mTextView = mContainer.getTextView();
		mImageView = mContainer.getImageView();
		mLength = mContainer.getLength();
		
		Bitmap rotated = rotate(bmp);
		
		//Getting the image in black-and-white
		publishProgress(PROG_GREY);
		Bitmap grey = doGreyscale(rotated);
		
		//Processing the image through the Sobel Filter (edge detector)
		publishProgress(PROG_GX);
		Bitmap gx = convolutionBitmap(grey, SOBEL_X);
		
		publishProgress(PROG_GY);
		Bitmap gy = convolutionBitmap(grey, SOBEL_Y);
		
		publishProgress(PROG_ADD);
		Bitmap out = addBitmap(gx, gy);
		
		//Compute string from the processed image
		publishProgress(PROG_STRING);
		int[][] intensity = intensityDetector(out);
		double[] distance = computeDistance(intensity);
		String password = computePswd(distance);
		
		//Store the password in the container 
		mContainer.setPassword(password);
		mContainer.setBitmap(out);
		
		return mContainer;
		
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		//Show and update the process dialog
		prog = values[0];
		if (prog == PROG_GREY) {
			progress.setMessage("Greyscaling... (2/5)");
		}
		else if (prog == PROG_GX) {
			progress.setMessage("Computing GX... (3/5)");
		}
		else if (prog == PROG_GY) {
			progress.setMessage("Computing GY... (4/5)");
		}
		else if (prog == PROG_ADD) {
			progress.setMessage("Adding the two... (5/5)");
		}
		else if (prog == PROG_STRING) {
			progress.setMessage("Computing string...");
		}
		progress.show();
	}
	
	@Override
	protected void onPostExecute(ResultsContainer container){
		//When the password is computed, stores it in the container,
		//then clean the screen.
		progress.dismiss();
		mImageView.setImageBitmap(container.getBitmap());
		mTextView.setText(container.getPassword());
	}
	
	///////////////////////////////////////////////////////////
	
	//Rotate the picture
	public static Bitmap rotate(Bitmap src){
		Matrix matrix = new Matrix();
	    // setup rotation degree
	    matrix.postRotate(90);
	    // return new bitmap rotated using matrix
	    return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
	}
	
	//Turn the picture in Black-and-White
	public static Bitmap doGreyscale(Bitmap src) {
	    // constant factors
	    final double GS_RED = 0.299;
	    final double GS_GREEN = 0.587;
	    final double GS_BLUE = 0.114;
	 
	    // create output bitmap
	    Bitmap bmOut = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
	    // pixel information
	    int A, R, G, B;
	    int pixel;
	 
	    // get image size
	    int width = src.getWidth();
	    int height = src.getHeight();
	 
	    // scan through every single pixel
	    for(int x = 0; x < width; ++x) {
	        for(int y = 0; y < height; ++y) {
	            // get one pixel color
	            pixel = src.getPixel(x, y);
	            // retrieve color of all channels
	            A = Color.alpha(pixel);
	            R = Color.red(pixel);
	            G = Color.green(pixel);
	            B = Color.blue(pixel);
	            // take conversion up to one single value
	            R = G = B = (int)(GS_RED * R + GS_GREEN * G + GS_BLUE * B);
	            // set new pixel color to output bitmap
	            bmOut.setPixel(x, y, Color.argb(A, R, G, B));
	        }
	    }
	 
	    // return final image
	    return bmOut;
	}
	
	//Apply a convolution matrix on the image
	public static Bitmap convolutionBitmap(Bitmap src, int test) {
		int width = src.getWidth();
		int height = src.getHeight();
		
		Bitmap output = Bitmap.createBitmap(width, height, src.getConfig());
		
		int A, R, G, B;
		int sumR, sumG, sumB;
		int[][] pixels = new int[3][3];
		int factor = 1;
		int offset = 0;
		double[][] matrix;
		
		if (test == SOBEL_X)
			matrix = SOBEL_KERNEL_X; 
		else
			matrix = SOBEL_KERNEL_Y;
		
        for(int y = 0; y < height - 2; ++y) {
            for(int x = 0; x < width - 2; ++x) {
 
                // get pixel matrix
                for(int i = 0; i < 3; ++i) {
                    for(int j = 0; j < 3; ++j) {
                        pixels[i][j] = src.getPixel(x + i, y + j);
                    }
                }
 
                // get alpha of center pixel
                A = Color.alpha(pixels[1][1]);
 
                // init color sum
                sumR = sumG = sumB = 0;
 
                // get sum of RGB on matrix
                for(int i = 0; i < 3; ++i) {
                    for(int j = 0; j < 3; ++j) {
                        sumR += (Color.red(pixels[i][j]) * matrix[i][j]);
                        sumG += (Color.green(pixels[i][j]) * matrix[i][j]);
                        sumB += (Color.blue(pixels[i][j]) * matrix[i][j]);
                    }
                }
 
                // get final Red
                R = (int)(sumR / factor + offset);
                if(R < 0) { R = 0; }
                else if(R > 255) { R = 255; }
 
                // get final Green
                G = (int)(sumG / factor + offset);
                if(G < 0) { G = 0; }
                else if(G > 255) { G = 255; }
 
                // get final Blue
                B = (int)(sumB / factor + offset);
                if(B < 0) { B = 0; }
                else if(B > 255) { B = 255; }
 
                // apply new pixel
                output.setPixel(x + 1, y + 1, Color.argb(A, R, G, B));
            }
        }
 
        // final image
        return output;
		
	}
	
	//Create a bitmap image by adding two images
	public static Bitmap addBitmap(Bitmap one, Bitmap two) {
	    // image size
	    int width = one.getWidth();
	    int height = one.getHeight();
	    // create output bitmap
	    Bitmap bmOut = Bitmap.createBitmap(width, height, one.getConfig());
	    // color information
	    int A, R, G, B;
	    int pixelOne, pixelTwo;
	 
	    // scan through all pixels
	    for(int x = 0; x < width; ++x) {
	        for(int y = 0; y < height; ++y) {
	            // get pixel color
	            pixelOne = one.getPixel(x, y);
	            pixelTwo = one.getPixel(x, y);
	            A = Color.alpha(pixelOne);
	            // apply filter contrast for every channel R, G, B
	            R = (int) Math.abs(Color.red(pixelOne))+Math.abs(Color.red(pixelTwo));
	            if(R < 0) { R = 0; }
	            else if(R > 255) { R = 255; }
	 
	            G = (int) Math.abs(Color.green(pixelOne)) + Math.abs(Color.green(pixelTwo));
	            if(G < 0) { G = 0; }
	            else if(G > 255) { G = 255; }
	 
	            B = (int) Math.abs(Color.blue(pixelOne)) + Math.abs(Color.blue(pixelTwo));
	            if(B < 0) { B = 0; }
	            else if(B > 255) { B = 255; }
	 
	            // set new pixel color to output bitmap
	            bmOut.setPixel(x, y, Color.argb(A, R, G, B));
	        }
	    }
	    return bmOut;
	}	
	
	//Store the coordinates of the pixels with the most intensity
	public static int[][] intensityDetector(Bitmap src){
		
	    // pixel information
	    int R;
	    int pixel;
	    int i = 0;
	 
	    // get image size
	    int width = src.getWidth();
	    int height = src.getHeight();
	    
	    int[][] tmp = new int[width * height][2];
	    
	    // scan through every single pixel
	    for (int x = 0; x < width; ++x) {
	        for (int y = 0; y < height; ++y) {
	            // get one pixel color
	            pixel = src.getPixel(x, y);
	            // retrieve color of red channel (image being B&W, all three channels are identical)
	            R = Color.red(pixel);
	            
	            if (R > 200){
	            	tmp[i][0] = x;
	            	tmp[i][1] = y;
	            	i++;
	            }

	        }
	    }
	    
	    int[][] result = new int[i][2];
	    for (int j = 0; j < i; j++) {
	    	result[j][0] = tmp[j][0];
	    	result[j][1] = tmp[j][1];
	    }
		
		return result;
	}
	
	//Compute the distance from the origin of every point
	public static double[] computeDistance(int[][] crd){
		
		double[] result = new double[crd.length];
		for (int k = 0; k < crd.length; k++)
			result[k] = 0;
		double distance;
		
		int j = 0;
	
		for (int i = 0; i < crd.length; i++){
			distance = Math.sqrt(Math.pow(crd[i][0], 2) + Math.pow(crd[i][1], 2));
			if (distance != 0) {
				result[j] = distance;
				j++;
			}
		}
		
		double test[] = new double[j];
		for (int l = 0; l < j; l++) {
				test[l] = result[l];
		}

		
		return result;
	}
	
	//Compute the password
	public String computePswd(double[] distances){
		
		String s = "";
		
		mPseudo = mContainer.getPseudo();
		double sum = 0;
		
		for (int j = 0; j < mPseudo.length(); j++){
			sum += (double) mPseudo.charAt(j);
		}
		sum = Math.floor((sum % distances.length) / mLength)-1;
		
		int test = (int) sum;
		
		for (int i = 0; i < mLength; i++) {

				s = s + charCompute(distances[i*test]);
		}

		return s;
		
	}
	
	//Computes a writable char from a double 
	public static char charCompute(double a) {

		if (a == 0)
			return (char) a;
		else if (a < CHAR_MIN) {
			a = a + CHAR_MIN;
			return (char) a;
		}
		else if (a > CHAR_MAX) {
			a = a % CHAR_MAX;
			return charCompute(a);
		}
		return (char) a;
	}
	
}
