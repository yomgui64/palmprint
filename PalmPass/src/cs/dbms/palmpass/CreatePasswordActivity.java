package cs.dbms.palmpass;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.Toast;


public class CreatePasswordActivity extends Activity{

    String domain = "";
    String pseudo = "";
    int length = 4;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_password);
	
		final EditText editDomain = (EditText) findViewById(R.id.editDomain);
		final EditText editPseudo = (EditText) findViewById(R.id.editPseudo);
		
		final DataHandler db = new DataHandler(this);
		
        // This button creates a new entry in the database containing the informations
		// given by the user
		final Button buttonNext = (Button) findViewById(R.id.nextCreate2Instructions);
        buttonNext.setOnClickListener(new View.OnClickListener() {
			
        	
			@Override
			public void onClick(View v) {

				domain = editDomain.getText().toString();
				pseudo = editPseudo.getText().toString();
				
				if (!domain.equals("") && !pseudo.equals("")){
					
					long id = db.addLogin(new DataLogin(pseudo, domain, length));	
					
					Intent intent = new Intent(CreatePasswordActivity.this, ImagePickActivity.class);
					intent.putExtra("LOGIN_ID", id);
					startActivity(intent);
				}
				else
					Toast.makeText(getApplicationContext(), "Please fill in the forms first", Toast.LENGTH_SHORT).show();
			}
		});	
        
       final NumberPicker picker = (NumberPicker) findViewById(R.id.numberPicker);
       
       //Options for the number picker
       picker.setMinValue(4);
       picker.setMaxValue(10);
       picker.setWrapSelectorWheel(false);
       
       picker.setOnValueChangedListener(new OnValueChangeListener() {

		@Override
		public void onValueChange(NumberPicker arg0, int oldVal, int newVal) {
			
			length = newVal;
		}
    	   
       });
        
	}
	
}
