package cs.dbms.palmpass;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

// Adapter class for the custom list in OldPasswordActivity
public class DataAdapter extends ArrayAdapter<DataLogin>{
	
	DataHandler _handler;
	
	public DataAdapter(Context context, DataHandler handler){
		super(context, R.layout.item_login, handler.getAllLogins());
		_handler = handler;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		//Get the data for this position
		final DataLogin login = getItem(position);
		//DataHandler handler = null;
		
		//Check if an existing view is being reused, otherwise inflate the view
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_login, parent, false);
		}
		
		//Lookup view for data population
		TextView Pseudo = (TextView) convertView.findViewById(R.id.loginPseudo);
		TextView Website = (TextView) convertView.findViewById(R.id.loginWebsite);
		
		//Populate the data into the template view using the data object
		Pseudo.setText(login.getPseudo());
		Website.setText(login.getWebsite());

        
		//Return the completed view to render on screen
		return convertView;
		
	}
	
}
