package cs.dbms.palmpass;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

// Handler class for the SQLite Database
public class DataHandler extends SQLiteOpenHelper {
	
	private static final String DB_NAME = "Database";
	private static final Integer DB_VERSION = 1;
	
	private static final String KEY_ID = "id";
	private static final String KEY_PSEUDO = "pseudo";
	private static final String KEY_WEBSITE = "website";
	private static final String KEY_LENGTH = "length";
	private static final String KEY_CRYPTO = "crypto";
	
	private static final String TABLE_NAME = "Login";
	
	public DataHandler(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + KEY_ID + " INTEGER PRIMARY KEY," 
																	   + KEY_PSEUDO + " TEXT,"
																	   + KEY_WEBSITE + " TEXT,"
																	   + KEY_LENGTH + " INTEGER,"
																	   + KEY_CRYPTO + " TEXT" + ")";
		
		db.execSQL(CREATE_LOGIN_TABLE);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_NAME);
		onCreate(db);
		
	}
	
	//Creating new Login, returns the id
	public long addLogin(DataLogin login){
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(KEY_PSEUDO, login.getPseudo());
		values.put(KEY_WEBSITE, login.getWebsite());
		values.put(KEY_LENGTH, login.getLength());
		values.put(KEY_CRYPTO, login.getCrypto());
		
		long id = db.insert(TABLE_NAME, null, values);
		db.close();
		return id;
	}
	
	//Getting a single login
	public DataLogin getLogin(long id){
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor cursor = db.query(TABLE_NAME, new String[] { KEY_ID,  KEY_PSEUDO,  KEY_WEBSITE, KEY_LENGTH, KEY_CRYPTO }, KEY_ID + "=?", 
				new String[] {String.valueOf(id)}, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();
		
		DataLogin login = new DataLogin(Integer.parseInt(cursor.getString(0)), 
										cursor.getString(1),
										cursor.getString(2),
										Integer.parseInt(cursor.getString(3)),
										cursor.getString(4));
		return login;
	}
	
	//Getting all logins
	public List<DataLogin> getAllLogins(){
		List<DataLogin> loginList = new ArrayList<DataLogin>();
		String selectQuery = "SELECT * FROM " + TABLE_NAME;
		
		// Writable or Readable ?
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		if (cursor.moveToFirst()) {
			do {
				DataLogin login = new DataLogin();
				login.setId(Integer.parseInt(cursor.getString(0)));
				login.setPseudo(cursor.getString(1));
				login.setWebsite(cursor.getString(2));
				login.setLength(Integer.parseInt(cursor.getString(3)));
				login.setCrypto(cursor.getString(4));
				loginList.add(login);
			} while (cursor.moveToNext());
		}
		db.close();
		return loginList;
	}
	
	//Getting number of logins currently in database
	public int getLoginCount(){
		String countQuery = "SELECT * FROM " + TABLE_NAME;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery,  null);
		cursor.close();
		
		return cursor.getCount();
	}
	
	//Updating login
	public int updateLogin(DataLogin login){
		SQLiteDatabase db = this.getReadableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(KEY_PSEUDO, login.getPseudo());
		values.put(KEY_WEBSITE, login.getWebsite());
		values.put(KEY_LENGTH, login.getLength());
		values.put(KEY_CRYPTO, login.getCrypto());
		
		return db.update(TABLE_NAME, values, KEY_ID + "=?", new String[] { String.valueOf(login.getId()) });

	}

	//Deleting login
	public void deleteLogin(DataLogin login){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_NAME, KEY_ID + " = ?", new String[] {String.valueOf(login.getId())});
		db.close();
		
	}
	
	
	
}
