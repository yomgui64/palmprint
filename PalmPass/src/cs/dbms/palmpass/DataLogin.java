package cs.dbms.palmpass;

public class DataLogin {

	private int _id;
	private String _pseudo = "johndoe";
	private String _website = "google.com";
	private int _length = 4;
	private String _crypto = ".";
	
	//Constructors
	public DataLogin(){
	}
	
	public DataLogin(String pseudo, String website, int length){
		this._pseudo = pseudo;
		this._website = website;
		this._length = length;
	}
	
	public DataLogin(int id, String pseudo, String website, int length){
		this._id = id;
		this._pseudo = pseudo;
		this._website = website;
		this._length = length;
	}
	
	public DataLogin(int id, String pseudo, String website, int length, String crypto){
		this._id = id;
		this._pseudo = pseudo;
		this._website = website;
		this._length = length;
		this._crypto = crypto;
	}
	
	//Getters
	public int getId(){
		return this._id;
	}
	
	public String getPseudo(){
		return this._pseudo; 
	}
	
	public String getWebsite(){
		return this._website;
	}
	
	public int getLength(){
		return this._length;
	}
	
	public String getCrypto(){
		return this._crypto;
	}
	
	//Setters
	public void setId(int id){
		this._id = id;
	}
	
	public void setPseudo(String pseudo){
		this._pseudo = pseudo;
	}
	
	public void setWebsite(String website){
		this._website = website;
	}
	
	public void setLength(int length){
		this._length = length;
	}
	
	public void setCrypto(String crypto){
		this._crypto = crypto;
	}
	
}
