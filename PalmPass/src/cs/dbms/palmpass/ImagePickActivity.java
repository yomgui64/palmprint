package cs.dbms.palmpass;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ImagePickActivity extends Activity {
	
	static final int REQUEST_IMAGE_CAPTURE = 1;
	public String mCurrentPhotoPath;
	long id = 1;
	
	static final int REQUEST_TAKE_PHOTO = 1;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_pick);
		
		final Button buttonTakePic = (Button) findViewById(R.id.nextInstru2Photo);
		final Button buttonNext = (Button) findViewById(R.id.nextPhoto2Passw);
		buttonNext.setEnabled(false);
		
		Bundle extras = getIntent().getExtras();
		if (extras!=null) {
			id = extras.getLong("LOGIN_ID");
		}
		
		// Button to take a picture
		buttonTakePic.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Toast.makeText(getApplicationContext(), "STARTING CAMERA", Toast.LENGTH_SHORT).show();
				dispatchTakePictureIntent();
				buttonNext.setEnabled(true);
			}
		});
		
		//Button to go to the next screen
		buttonNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				Intent intent = new Intent(getApplicationContext(), PasswordComputationActivity.class);
				intent.putExtra("PHOTO_PATH", mCurrentPhotoPath);
				intent.putExtra("LOGIN_ID", id);
				startActivity(intent);
				}
			
		});
	}
	
	private void dispatchTakePictureIntent() {
	    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    // Ensure that there's a camera activity to handle the intent
	    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
	        // Create the File where the photo should go
	        File photoFile = null;
	        try {
	            photoFile = createImageFile();
	        } catch (IOException ex) {
	        	Toast.makeText(getApplicationContext(), "Error in creating file", Toast.LENGTH_SHORT).show();
	        }
	        // Continue only if the File was successfully created
	        if (photoFile != null) {
	            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
	                    Uri.fromFile(photoFile));
	            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
	        }
	    }
	}
	

	 private File createImageFile() throws IOException {
		    // Create an image file name
		    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		    String imageFileName = "JPEG_" + timeStamp + "_";
		    
		    File storageDir = Environment.getExternalStorageDirectory();
		    File image = File.createTempFile(
		        imageFileName,  /* prefix */
		        ".jpg",         /* suffix */
		        storageDir      /* directory */
		    );

		    // Save a file: path for use with ACTION_VIEW intents
		    mCurrentPhotoPath = image.getAbsolutePath();
		    return image;
		}
}
