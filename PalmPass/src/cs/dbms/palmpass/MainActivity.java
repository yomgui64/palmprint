package cs.dbms.palmpass;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

// Activity for the start screen
public class MainActivity extends ActionBarActivity {

	public final static String EXTRA_MESSAGE = "cs.dbms.palmpass.MESSAGE";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // Button "Generate New Password"
        Button b_new = (Button) findViewById(R.id.startNewPassApp);
        b_new.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent_n = new Intent(MainActivity.this, CreatePasswordActivity.class);
				startActivity(intent_n);
			}
		});
        
        // Button "Find Previous Passwords"
        Button b_old = (Button) findViewById(R.id.startOldPassApp);
        b_old.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent_n = new Intent(MainActivity.this, OldPasswordActivity.class);
				startActivity(intent_n);
			}
		});
        
        // Button "Options"
        Button b_opt = (Button) findViewById(R.id.startOptions);
        b_opt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent_n = new Intent(MainActivity.this, OptionsActivity.class);
				startActivity(intent_n);
			}
		});
        
    
    }

}
