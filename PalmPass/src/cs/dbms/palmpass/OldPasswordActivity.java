package cs.dbms.palmpass;

import java.util.List;
import java.util.ListIterator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class OldPasswordActivity extends Activity {
	
	private DataHandler data = new DataHandler(this);
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_password);
   
		List<DataLogin> loginlist = data.getAllLogins();
		ListIterator<DataLogin> it = loginlist.listIterator() ;
		while(it.hasNext()){
			DataLogin loginTest = it.next();
			String test = loginTest.getCrypto();
			if (test.equals(".")){
				data.deleteLogin(loginTest);
			}
		}
        DataAdapter adapter = new DataAdapter(this, data);
        
        final ListView listContent = (ListView)findViewById(R.id.listView1);
        listContent.setAdapter(adapter);
        
        listContent.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long id) {

				DataLogin login = (DataLogin) listContent.getItemAtPosition(position);
				int login_id = login.getId();
	
				Intent intent = new Intent(OldPasswordActivity.this, OldPasswordDisplayActivity.class);
				intent.putExtra("DATA_LOGIN", login_id);
				startActivity(intent);
			}
        	
        });
        
	}

	
}
