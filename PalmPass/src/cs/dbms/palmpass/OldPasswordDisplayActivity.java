package cs.dbms.palmpass;

import java.io.UnsupportedEncodingException;

import android.app.Activity;
import android.os.Bundle;
import android.util.Base64;
import android.widget.TextView;

public class OldPasswordDisplayActivity extends Activity {

	long id = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_old_password_display);
		
		Bundle extras = getIntent().getExtras();
		if (extras!=null) {
			id = (long) extras.getInt("DATA_LOGIN");
		}
		
		DataHandler handler = new DataHandler(this);
		DataLogin login = handler.getLogin(id);
		
		String password = login.getCrypto();
		
		byte[] data = Base64.decode(login.getCrypto(), Base64.DEFAULT);
		try {
			password = new String(data, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		final TextView domain_view = (TextView) findViewById(R.id.view_display_domain);
		final TextView pseudo_view = (TextView) findViewById(R.id.view_display_pseudo);
		final TextView password_view = (TextView) findViewById(R.id.view_display_password);
		
		domain_view.setText(login.getWebsite());
		pseudo_view.setText(login.getPseudo());
		password_view.setText(password);
		
	}
	
}
