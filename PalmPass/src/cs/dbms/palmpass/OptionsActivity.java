package cs.dbms.palmpass;

import java.util.List;
import java.util.ListIterator;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class OptionsActivity extends Activity
				implements OptionsDialog.NoticeDialogListener{
	
	private DataHandler data = new DataHandler(this);
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_options);
    final OptionsDialog dialog = new OptionsDialog();
    
    // Button "Delete Passwords"
    Button b = (Button) findViewById(R.id.delete_button);
    b.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			//Show dialog message on screen
			dialog.show(getFragmentManager(), "delete");
		}
	});
    
	}

	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		// Clean the database
		List<DataLogin> loginlist = data.getAllLogins();
		ListIterator<DataLogin> it = loginlist.listIterator() ;
		while(it.hasNext()){
			data.deleteLogin(it.next());
		}
		Toast.makeText(getApplicationContext(), "Passwords deleted", Toast.LENGTH_SHORT).show();
		Intent intent = new Intent(OptionsActivity.this, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
		startActivity(intent);
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// (nothing happens)
		
	}
	

}
