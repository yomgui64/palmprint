package cs.dbms.palmpass;

import java.io.File;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PasswordComputationActivity extends Activity {

	private DataHandler data;
	private DataLogin login;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_password_comp);
		
		Bundle extras = getIntent().getExtras();
		String mCurrentPhotoPath = null;
		long id = 0;
		int length = 4;
		if (extras!=null) {
			mCurrentPhotoPath = extras.getString("PHOTO_PATH");
			id = extras.getLong("LOGIN_ID");
			length = getLengthPassword(id);
		}
		
		final ImageView view = (ImageView) findViewById(R.id.imageView);
		final TextView text = (TextView) findViewById(R.id.textResult);
		
		data = new DataHandler(this);
		login = data.getLogin(id);
		
		// Button to encrypt and record password in database
		final Button button = (Button) findViewById(R.id.buttonOKPassword);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				String passText = text.getText().toString();
				String encryption = "";
				try {
					encryption = encodingPassword(passText);			
					} catch (Exception e) {
					e.printStackTrace();
				}
				login.setCrypto(encryption);
				data.updateLogin(login);
				Toast.makeText(getApplicationContext(), "Password Registered", Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(PasswordComputationActivity.this, MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
				startActivity(intent);
			}
			
		});
		
		String pseudo = login.getPseudo();
		
		// Find and decode the picture taken in ImagePickActivity
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.outHeight = 50;
		options.outWidth = 50;
		options.inSampleSize = 4;
		final File file = new File(mCurrentPhotoPath);
		final Bitmap picture = BitmapFactory.decodeFile(mCurrentPhotoPath, options);
		
		// Put all the attributes needed for computation in container
		final ResultsContainer results = new ResultsContainer(picture, view, text, length, pseudo);
		
		// Starts computation
		BitmapAndPasswordTask task = new BitmapAndPasswordTask(PasswordComputationActivity.this, results);
		task.execute();
		
		// Delete picture file
		file.delete();
	
	}
	
	int getLengthPassword(long id){
		final DataHandler db = new DataHandler(this);
		DataLogin login = db.getLogin(id);
		return login.getLength();
	}
	
	// Encode the password
	String encodingPassword(String password) throws Exception{
		
		byte[] data = password.getBytes("UTF-8");
		return Base64.encodeToString(data, Base64.DEFAULT);
		
	}

}

