package cs.dbms.palmpass;

import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.TextView;

// A container class whose only purpose is to give more than one argument
// to the asynchronous BitmapAndPasswordTask.

public class ResultsContainer {

	String _password = null;
	Bitmap _bmp;
	TextView _textView;
	ImageView _imageView;
	int _length;
	String _pseudo;
	
	ResultsContainer(Bitmap bmp, ImageView view, TextView text, int length, String pseudo) {
		_bmp = bmp;
		_imageView = view;
		_textView = text;
		_length = length;
		_pseudo = pseudo;
	}
	
	//Getters
	Bitmap getBitmap(){
		return _bmp;
	}
	
	String getPassword(){
		return _password;
	}
	
	TextView getTextView(){
		return _textView;
	}
	
	ImageView getImageView(){
		return _imageView;
	}
	
	int getLength(){
		return _length;
	}
	
	String getPseudo(){
		return _pseudo;
	}
	
	//Setters
	void setPassword(String password){
		_password = password;
	}
	
	void setBitmap(Bitmap bmp){
		_bmp = bmp;
	}
	
	void setTextView(TextView text){
		_textView = text;
	}
	
	void setImageView(ImageView view){
		_imageView = view;
	}
	
	void setLength(int length){
		_length = length;
	}
	
	void setPseudo(String pseudo){
		_pseudo = pseudo;
	}
}
